package ae.android.sematec.VisibleTest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ae.android.sematec.MyViews.MyImageView;
import ae.android.sematec.R;

public class VisibleTestActivity extends AppCompatActivity {
MyImageView visibleTest;
Button toggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visible_test);

        visibleTest = findViewById(R.id.visibleTest);
        visibleTest.drawableLoader(R.drawable.suny);

        findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(visibleTest.getVisibility()==View.VISIBLE)
                    visibleTest.setVisibility(View.INVISIBLE);
                else
                    visibleTest.setVisibility(View.VISIBLE);
            }
        });

    }
}
