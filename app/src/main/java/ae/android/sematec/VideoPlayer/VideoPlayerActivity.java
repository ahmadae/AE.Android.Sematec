package ae.android.sematec.VideoPlayer;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.PermissionRequest;
import android.widget.MediaController;
import android.widget.VideoView;

import ae.android.sematec.R;

public class VideoPlayerActivity extends AppCompatActivity {

BroadcastReceiver callReceiver;
    VideoView myVideo;
   String url = "https://hw18.asset.aparat.com/aparat-video/7a1a7a58e8cece5d095d3dbe355c27129569692-144p__19652.mp4";
  //  String url ="https://hw17.asset.aparat.com/aparat-video/9540ce50b7ba59db0a43be066b6fed449570392-144p__69525.mp4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        myVideo = findViewById(R.id.myVideo);
        checkPermissions();
        myVideo.setMediaController(new MediaController(this));
        myVideo.setVideoURI(Uri.parse(url));
        myVideo.start();



        callReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(myVideo.isPlaying())
                    myVideo.pause();
            }
        };

        IntentFilter callFilter = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(callReceiver,callFilter);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(callReceiver);
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE}, 1800);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1800) {

        }
    }
}
