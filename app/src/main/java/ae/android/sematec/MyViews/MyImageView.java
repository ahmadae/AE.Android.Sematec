package ae.android.sematec.MyViews;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;

/**
 * Created by AN on 02/13/2018.
 */

public class MyImageView extends AppCompatImageView {
    Context context;

    public MyImageView(Context context) {
        super(context);
        this.context = context;
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void drawableLoader(int i) {
        Glide.with(context).load(i).into(this);
    }

    public void urlLoader(String url) {
        Glide.with(context).load(url).into(this);
    }

}
