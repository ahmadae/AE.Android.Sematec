package ae.android.sematec.MyViews;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by AN on 02/13/2018.
 */

public class MyButton extends AppCompatButton {
    Context context;
    public MyButton(Context context) {
        super(context);
        this.context=context;
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
    }

}
