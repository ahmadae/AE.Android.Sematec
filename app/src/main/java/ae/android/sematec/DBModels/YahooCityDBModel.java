package ae.android.sematec.DBModels;

import com.orm.SugarRecord;

/**
 * Created by AN on 02/09/2018.
 */

public class YahooCityDBModel extends SugarRecord<YahooCityDBModel> {

    String cityName;
    String temp;

    public YahooCityDBModel() {
    }

    public YahooCityDBModel(String cityName, String temp) {
        this.cityName = cityName;
        this.temp = temp;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
