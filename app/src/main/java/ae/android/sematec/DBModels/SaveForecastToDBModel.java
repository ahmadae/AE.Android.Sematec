package ae.android.sematec.DBModels;

import com.orm.SugarRecord;

/**
 * Created by AN on 02/12/2018.
 */

public class SaveForecastToDBModel extends SugarRecord<SaveForecastToDBModel> {


    private String cityName;
    private String date;
    private String day;
    private String high;
    private String low;
    private String text;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public SaveForecastToDBModel() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
