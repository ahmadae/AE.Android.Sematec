package ae.android.sematec.DBModels;

import com.orm.SugarRecord;

/**
 * Created by AN on 02/14/2018.
 */

public class ChargerPlugDBModel extends SugarRecord<ChargerPlugDBModel> {
    String city;
    String temp;

    public ChargerPlugDBModel(String city, String temp) {
        this.city = city;
        this.temp = temp;
    }

    public ChargerPlugDBModel() {
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
