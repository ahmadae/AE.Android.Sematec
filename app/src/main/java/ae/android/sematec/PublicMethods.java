package ae.android.sematec;

/**
 * Created by AN on 02/07/2018.
 */

public class PublicMethods {

    public static String convertFtoC(String fStr)
    {
        double d = Double.parseDouble(fStr);
        double doubleResult = (d - 32) / 1.8;
        return (int)doubleResult+"°C";

    }
}
