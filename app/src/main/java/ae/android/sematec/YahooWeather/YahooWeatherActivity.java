package ae.android.sematec.YahooWeather;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.List;

import ae.android.sematec.DBModels.SaveForecastToDBModel;
import ae.android.sematec.DBModels.YahooCityDBModel;
import ae.android.sematec.PublicMethods;
import ae.android.sematec.R;
import ae.android.sematec.YahooWeather.Models.Forecast;
import ae.android.sematec.YahooWeather.Models.YahooWeatherModel;
import cz.msebera.android.httpclient.Header;

public class YahooWeatherActivity extends AppCompatActivity implements View.OnClickListener, View.OnKeyListener, TextView.OnEditorActionListener {

    TextView result;
    EditText cityName;
    Button show, deletDB;
    ListView foreCastList;


    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setVisible(false);
        setContentView(R.layout.activity_yahoo_weather);
        bindViews();
        show.setOnClickListener(this);
        deletDB.setOnClickListener(this);


    }

    void bindViews() {
        result = findViewById(R.id.result);
        cityName = findViewById(R.id.cityName);
        show = findViewById(R.id.show);
        deletDB = findViewById(R.id.dleteDB);
        foreCastList = findViewById(R.id.foreCastList);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait");

        cityName.setOnEditorActionListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.show) {
            String cityValue = cityName.getText().toString();
            getDataFromYahoo(cityValue);
        } else if (view.getId() == R.id.dleteDB) {

            String delQuery = "cityName = 'Tehran'";

//

            try {
                List<YahooCityDBModel> yahooCityDBModels = YahooCityDBModel.listAll(YahooCityDBModel.class);
                YahooCityDBModel.deleteAll(YahooCityDBModel.class);

                Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {

                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();

            }


        }
    }

    private void getDataFromYahoo(String cityValue) {
        progressDialog.show();
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"
                + cityValue + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        final AsyncHttpClient yahooClient = new AsyncHttpClient();
        yahooClient.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(YahooWeatherActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                parseData(responseString);


            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressDialog.dismiss();
            }
        });
    }


    private void parseData(String responseString) {
        Gson yahooGson = new Gson();
        try {
            YahooWeatherModel yahooModel = yahooGson.fromJson(responseString, YahooWeatherModel.class);
            String resultValue = yahooModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp();


            result.setText(cityName.getText().toString() + " : " + PublicMethods.convertFtoC(resultValue));

            saveToCityDB(cityName.getText().toString(), resultValue);

            saveToForecastDB(cityName.getText().toString(), yahooModel.getQuery().getResults().getChannel().getItem().getForecast());

            Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();


            ForeCastAdapter foreCastAdapter = new ForeCastAdapter(this,
                    yahooModel.getQuery().getResults().getChannel().getItem().getForecast());
            foreCastList.setAdapter(foreCastAdapter);
        } catch (Exception e) {

            Toast.makeText(this, "Not Success", Toast.LENGTH_SHORT).show();

            result.setText("");
            foreCastList.setAdapter(null);

        }

    }

    private void saveToForecastDB(String city, List<Forecast> forecasts) {

        for (Forecast frc : forecasts) {
            SaveForecastToDBModel saveForecast = new SaveForecastToDBModel();
            saveForecast.setCityName(city);
            saveForecast.setDate(frc.getDate());
            saveForecast.setDay(frc.getDay());
            saveForecast.setHigh(frc.getHigh());
            saveForecast.setLow(frc.getLow());
            saveForecast.setText(frc.getText());

            saveForecast.save();

        }

    }


    private void saveToCityDB(String cityName, String resultValue) {
        YahooCityDBModel cityDB = new YahooCityDBModel(cityName, resultValue);
        cityDB.setCityName(cityName);
        cityDB.setTemp(resultValue);
        cityDB.save();


    }


    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        return false;
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        String cityValue = cityName.getText().toString();
        getDataFromYahoo(cityValue);
        return false;
    }
}
