package ae.android.sematec.YahooWeather;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import ae.android.sematec.MyViews.MyImageView;
import ae.android.sematec.MyViews.MyTextView;
import ae.android.sematec.PublicMethods;
import ae.android.sematec.R;
import ae.android.sematec.YahooWeather.Models.Forecast;

/**
 * Created by AN on 02/07/2018.
 */

public class ForeCastAdapter extends BaseAdapter {
    Context mContext;
    String cityName;
    List<Forecast> foreCast = new ArrayList<>();




    public ForeCastAdapter(Context mContext, List<Forecast> foreCast) {
        this.mContext = mContext;
        this.foreCast = foreCast;
    }


    @Override
    public int getCount() {
        return foreCast.size();
    }

    @Override
    public Object getItem(int i) {
        return foreCast.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View forCastView = LayoutInflater.from(mContext).inflate(R.layout.forecast_list_items, null);

        MyTextView dateValue = forCastView.findViewById(R.id.date);
        MyTextView dayValue = forCastView.findViewById(R.id.day);
        MyTextView highValue = forCastView.findViewById(R.id.high);
        MyTextView lowValue = forCastView.findViewById(R.id.low);
        MyTextView textValue = forCastView.findViewById(R.id.text);
        MyImageView weatherImage = forCastView.findViewById(R.id.weatherImage);

        Forecast castValue = foreCast.get(i);


        dateValue.setText("Date : " + castValue.getDate());
        dayValue.setText("Day : " + castValue.getDay());
        highValue.setText("High : " + PublicMethods.convertFtoC(castValue.getHigh()));
        lowValue.setText("Low : " + PublicMethods.convertFtoC(castValue.getLow()));
        textValue.setText("Text : " + castValue.getText());




        if (castValue.getText().toLowerCase().contains("snow"))
         //   Picasso.with(mContext).load(R.drawable.snow).into(weatherImage);
            weatherImage.drawableLoader(R.drawable.snow);
        else if (castValue.getText().toLowerCase().contentEquals("sunny"))
           // Picasso.with(mContext).load(R.drawable.suny).into(weatherImage);
            weatherImage.drawableLoader(R.drawable.suny);
        else if (castValue.getText().toLowerCase().contains("shower"))
           // Picasso.with(mContext).load(R.drawable.showers).into(weatherImage);
            weatherImage.drawableLoader(R.drawable.showers);
        else if (castValue.getText().toLowerCase().contains("partly") ||
                castValue.getText().toLowerCase().contains("mostly"))
          //  Picasso.with(mContext).load(R.drawable.partly_cloudy).into(weatherImage);
            weatherImage.drawableLoader(R.drawable.partly_cloudy);
        else if (castValue.getText().toLowerCase().contentEquals("cloudy"))
        //  Picasso.with(mContext).load(R.drawable.cloudy).into(weatherImage);

            weatherImage.drawableLoader(R.drawable.cloudy);


        return forCastView;

    }



}