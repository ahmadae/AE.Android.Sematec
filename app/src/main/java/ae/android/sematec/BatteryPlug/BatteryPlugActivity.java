package ae.android.sematec.BatteryPlug;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import ae.android.sematec.MyViews.MyTextView;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.List;

import ae.android.sematec.DBModels.ChargerPlugDBModel;
import ae.android.sematec.PublicMethods;
import ae.android.sematec.R;
import ae.android.sematec.YahooWeather.Models.YahooWeatherModel;
import cz.msebera.android.httpclient.Header;

import static android.os.BatteryManager.EXTRA_LEVEL;

public class BatteryPlugActivity extends AppCompatActivity {


    BroadcastReceiver BatteryChange;
    TextView batteryLevel;
    ProgressBar batteryProgress;
    BatteryView bv;
    TextView valueCheckNet;
    TextView temp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battery_plug);

        bindViews();


        BatteryChange = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                batteryLevel.setText("Buttery level is : " + level + "%");

                batteryLevelSetColor(level);

                bv.setmLevel(level);

            }
        };

        IntentFilter batteryFilter = new IntentFilter("android.intent.action.BATTERY_CHANGED");
        registerReceiver(BatteryChange, batteryFilter);


        getDataFromYahoo();
    }

    private void batteryLevelSetColor(int level) {
        if (level < 40)
            batteryLevel.setTextColor(getResources().getColor(R.color.batterylow));
        else
            batteryLevel.setTextColor(getResources().getColor(R.color.batterygood));
    }


    private void bindViews() {
        batteryLevel = findViewById(R.id.batteryLevel);
        // batteryProgress = findViewById(R.id.batteryProgress);
        bv = findViewById(R.id.batteryview);
        valueCheckNet = findViewById(R.id.checkNet);
        temp = findViewById(R.id.temp);

    }

    void getDataFromYahoo() {
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                "Tehran%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        final AsyncHttpClient yahooClient = new AsyncHttpClient();
        yahooClient.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                valueCheckNet.setTextColor(getResources().getColor(R.color.batterylow));
                valueCheckNet.setText("Offline");
             try {
                // temp.setText(selectFromDB());
                  selectFromDB();
             }
                catch (Exception e){
                    Toast.makeText(BatteryPlugActivity.this,e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parsData(responseString);
                valueCheckNet.setTextColor(getResources().getColor(R.color.batterygood));
                valueCheckNet.setText("Online");


            }
        });

    }




    private void parsData(String responseString) {
        Gson yahooGson = new Gson();
        YahooWeatherModel yahooModel = yahooGson.fromJson(responseString, YahooWeatherModel.class);
        String result = yahooModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        String d = PublicMethods.convertFtoC(result);
        temp.setText(d);
      //  deleteFromDB();
        saveToDB(d);

    }

    private void deleteFromDB() {
        ChargerPlugDBModel chargerPlugDBModel = ChargerPlugDBModel.findById(ChargerPlugDBModel.class, (long) 1);
        chargerPlugDBModel.delete();

    }

    private void saveToDB(String temp) {

        ChargerPlugDBModel chargerPlugDBModel = new ChargerPlugDBModel("Tehran",temp);

        chargerPlugDBModel.save();


    }
    private void selectFromDB()
    {

       List<ChargerPlugDBModel> tempList =ChargerPlugDBModel.find(ChargerPlugDBModel.class,null,null);
        String tempVal = null;
        for (ChargerPlugDBModel ch :tempList
             ) {
            tempVal = ch.getTemp();
            temp.setText(ch.getTemp());

        }





    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(BatteryChange);
    }
}
