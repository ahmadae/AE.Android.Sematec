package ae.android.sematec.IMDB;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import ae.android.sematec.R;
import cz.msebera.android.httpclient.Header;

public class IMDBActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView poster;
    TextView title;
    TextView director;
    TextView plot;
    EditText movieName;
    Button show;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imdb);
        progressDialog = new ProgressDialog(IMDBActivity.this);
        bindViews();
        show.setOnClickListener(this);

    }

    void bindViews() {
        poster = findViewById(R.id.poster);
        title = findViewById(R.id.title);
        director = findViewById(R.id.director);
        plot = findViewById(R.id.plot);
        movieName = findViewById(R.id.movieName);
        show = findViewById(R.id.show);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait To Loading");


    }

    @Override
    public void onClick(View view) {

        getDataFromImdb(movieName.getText().toString());

    }

    void getDataFromImdb(String movieName) {
        progressDialog.show();
        String url = "http://www.omdbapi.com/?t=" + movieName + "&apikey=70ad462a";
        AsyncHttpClient imdbClient = new AsyncHttpClient();
        imdbClient.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Toast.makeText(IMDBActivity.this, "Sorry Failed To Find Movie", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                getJsonData(responseString);


            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressDialog.dismiss();
            }
        });
    }

    void getJsonData(String serverResponse) {

        Gson gsonImdb = new Gson();
        ImdbModel imdbModel = gsonImdb.fromJson(serverResponse, ImdbModel.class);

        title.setText(imdbModel.getTitle());
        director.setText(imdbModel.getDirector());
            plot.setText(imdbModel.getPlot());

            Picasso.with(this).load(imdbModel.getPlot()).into(poster);
//
//        try {
//            JSONObject allObject = new JSONObject(serverResponse);
//
//            String titleValue = allObject.getString("Title");
//            String directorValue=allObject.getString("Director");
//            String plotValue = allObject.getString("Plot");
//            String posterValue = allObject.getString("Poster");
//
//            title.setText(titleValue);
//            director.setText(directorValue);
//            plot.setText(plotValue);
//
//            Picasso.with(this).load(posterValue).into(poster);
//
//
//        } catch (JSONException e) {
//            Toast.makeText(this, "Failed Find Movie", Toast.LENGTH_SHORT).show();
//        }

    }


}
